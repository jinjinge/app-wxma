// index.js
// 获取应用实例
const app = getApp()

Page({
  data: {
    motto: 'Hello World',
    userInfo: {},
    hasUserInfo: false,
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
    canIUseGetUserProfile: false,
    canIUseOpenData: false //wx.canIUse('open-data.type.userAvatarUrl') && wx.canIUse('open-data.type.userNickName') // 如需尝试获取用户信息可改为false
  },
  // 事件处理函数
  bindViewTap() {
    wx.navigateTo({
      url: '../logs/logs'
    })
  },
  onLoad() {
    if (wx.getUserProfile) {
      this.setData({
        canIUseGetUserProfile: true
      })
    }
  },
  getUserProfile(e) {
    const _this = this
    // 推荐使用wx.getUserProfile获取用户信息，开发者每次通过该接口获取用户个人信息均需用户确认，开发者妥善保管用户快速填写的头像昵称，避免重复弹窗
    wx.getUserProfile({
      desc: '展示用户信息', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
      lang: 'zh_CN',
      success: (res) => {
        console.log(res)
        wx.request({
          method: "POST",
          url: getApp().globalData.apiUrl+"/api/auth/userinfo-wxma/wx432d37972042d2f7",
          header: {
            'Authorization': 'Bearer ' + app.globalData.authToken,  // 鉴权token
            'content-type': 'application/x-www-form-urlencoded'     // 模拟form表单提交
          },
          data: {
            encryptedData: res.encryptedData,
            signature: res.signature,
            rawData: res.rawData,
            iv: res.iv
          },
          success(res) {
            console.log('get userinfo success', res)
            getApp().globalData.appUser = res.data.data
            _this.setData({
              hasUserInfo: true,
              userInfo: {
                avatarUrl: res.data.data.headimgurl,
                nickName: res.data.data.nickname
              }
            })
          },
          fail(err) {
            console.error('err', err)
          }
        })
      },
      fail: err => {
        console.log('err', err)
      }
    })
  },
  getUserInfo(e) {
    // 不推荐使用getUserInfo获取用户信息，预计自2021年4月13日起，getUserInfo将不再弹出弹窗，并直接返回匿名的用户个人信息
    console.log(e)
    this.setData({
      userInfo: e.detail.userInfo,
      hasUserInfo: true
    })
  }
})
