# APP-wxma
Coadmin的微信小程序app演示工程。

使用Coadmin中app-api项目中的api，可以微信小程序登录，获取昵称和头像。

Coadmin：

     https://gitee.com/jinjinge/coadmin
     https://github.com/jinjingmail/coadmin

# 微信小程序测试方法
1） 登录网页，创建测试号

  https://developers.weixin.qq.com/miniprogram/dev/devtools/sandbox.html

  https://mp.weixin.qq.com/wxamp/sandbox?doc=1

2） 使用微信开发者工具，创建小程序，在“创建小程序”界面，选择“测试号”
