// app.js
App({
  onLaunch() {
    // 展示本地存储能力
    const logs = wx.getStorageSync('logs') || []
    logs.unshift(Date.now())
    wx.setStorageSync('logs', logs)

    // 登录
    wx.login({
      success: res => {
        // 发送 res.code 到后台换取 openId, sessionKey, unionId
        wx.request({
          method: "POST",
          url: getApp().globalData.apiUrl+"/api/auth/login-wxma/wx432d37972042d2f7",
          header: {
            'content-type': 'application/x-www-form-urlencoded' // 模拟form表单提交
          },
          data: {
            code: res.code,
            state: res.state
          },
          success(res) {
            console.log('login success', res)
            getApp().globalData.authToken = res.data.data.token
            getApp().globalData.appUser = res.data.data.appUser
          },
          fail(err) {
            console.error('err', err)
          }
        })
      },
      fail: err => {
        console.log('err', err)
      }
    })
  },
  globalData: {
    softVersion: '1.0.0',
    apiUrl: 'http://127.0.0.1:8006',
    authToken: null,
    appUser: {}
  }
})
